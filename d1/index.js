console.log("Hello World");

//Conditional Statements
	/*
		one of the key features of the prog language.
		in our daily lives, we decide on a daily basis.
		programming also needs to execute dicision making task.

		there are 3 types of conditional statemenr
			if-else
			swicth
			try-catch-finally
	*/


//What are conditional statements?
	/*
		- Conditional Statements allow us to control th flow of our program.
		- It allows us to run a statement or intruction if a condition is met or run another seperate instruction if otherwise.
	*/


	//if, else if, and else statement
	let numA = -1;
	if(numA < 0) {
		console.log("Yes! It is less than zero");
	}
	/*
		IF statement.
		executes a statement if a specified condition is true

		Syntax:
		if(condition) {
			statement;
		}
	*/

	console.log(numA < 0);	//true
	//The result of the expression added in the if's condition must result to true, else, the statement inside if() will not run.

	numA = 0;
	if(numA < 0){
		console.log("Hello");
	};
	console.log(numA < 0);	//false

	let city = "New York";
	if(city === "New York"){
		console.log("Welcome to New York City");
	};



//Else If Clause
	/*
		- Executes a statement if previous conditions are false and if the specified condition is tru
		- The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program.
	*/
	let numH = 1;
	if(numH < 0) {
		console.log("Hello");
	} else if(numH > 0) {
		console.log("World");
	}
	//We were able to run the else-if statement after we evaluated that the if condition was failed.
	//if the if() condition was passed and run, we will no longer evaluate the else-if() and end the process there.
	let numF = -2;
	if(numF < 0) {
		console.log("Hello");
	} else if(numF > 0) {
		console.log("World");
	}
	//else if() statement was no longer run because the if statement was able to run, the evaluation of the whole statement stops there.
	city = "Tokyo";
	if(city = "New York") {
		console.log("Welcome to New York City");
	} else if(city = "Tokyo") {
		console.log("Welcome to Tokyo, Japan!");
	};
	//since we failed the condition for the first if(), we went to the else-if() and checked and instead passed that condition.



//Else Statement
	/*
		- Executes a statement if all other conditoins are false.
		- The "else" statement is optional and can be added to capture any other result to change the flow of a program.
	*/
/*	if(numA < 0) {
		console.log("Hello");
	} else if (numH === 0); {
		console.log("World");
	} else {
		console.log("Again");
	};*/
	/*
		Since both the preceeding if and else if conditions are not met or failed, the else statement was run instead

		Else statement should only be added if there is a preceeding if condition.

		else statement by itself will not work, however, if statements will work even if there is no else statement.
	*/
	/*else{
		console.log("Will not run wothout an if");
	}*/	//this will result to an error.

	/*else if(numH === 0){
		console.log("world");
	} else {
		console.log("Again");
	}*/	//this will result to an error.

	//same goes for an else if, there should be a preceeding if() first.



//If, else if, and else statements with funnctions.
	/*
		- most of the times, we would like to use if, else if, and else statements with functions to control the flow of our application
	*/
	let message = "No message.";
	console.log(message);
	function determineTyphoonIntensity(windSpeed) {
		if(windSpeed < 30){
			return "Not a typhoon yet.";
		} else if(windSpeed <=61) {
			return "Tropical Depression Detected.";
		} else if(windSpeed >= 62 && windSpeed <= 88) {
			return "Tropical Storm Detected";
		} else if(windSpeed >= 89 || windSpeed <= 117) {
			return "Severe tropical storm detected."
		} else {
			return "Typhoon detected"
		}
	}
	message = determineTyphoonIntensity(110);
	console.log(message);
	//this return the string to the variable "message" that invoked it.

	if(message === "Severe tropical storm detected."){
		console.warn(message);
	}
	//console.warn() is a good way to print warnings in our console that could help us developers act on certain output with out code.


//Mini activity
//make a function that can check whether a number is odd or even with one parameter
/*function oddEvenChecker(x) {
	let remainder = x % 2;
	if(remainder === 0) {
		alert("Your number is even");
	} else if(remainder !== 0) {
		alert("Your number is odd");
	}
};
oddEvenChecker(7);

function ageChecker(age){
	if(age <= 18) {
		alert(age+" Not Allowed to Drink!");
		return(false);
	} else {
		alert(age+" shot na!");
		return true;
	};
}
let isAllowedToDrink = ageChecker(17);
console.log(isAllowedToDrink);*/



//Truthy and Falsy
	/*
		- In JavaScript a "truthy" value is a value that is considered tru when encountered in Boolean context
		- Values in are considered tru unless defined otherwide
		- Falsy Value or exeptions for thruth:
			1. false
			2. 0
			3. -0
			4. " "
			5. null
			6. undefined
			7. Nan

		- Truthy Examples
		if the result of an expression in a condition results to a truthy value, the condition returns tru and the corresponding statements are executed
		Expressions are any unit of code that can be evaluated to a value.

	*/

	//Truthy Examples
	if(true){
		console.log("Truthy");
	}
	if(1){
		console.log("Truthy");
	}
	if([2,3,4]){
		console.log("Truthy");
	}

	//Falsy Examples
	if(false){
		console.log("falsy");
	}
	if(0){
		console.log("falsy");
	}
	if(undefined){
		console.log("falsy");
	}



//Conditional or Ternary Operator
	/*
		- Ternary Operator takes in three operands:
			1. condition
			2. expression to execute if the condition is truthy.
			3. expression to execute if the condition is falsy.

		- This can be used as an alternative to an "if else" statement.
		- ternary operators have an implicit "return" statement, meaning that without the "return" keyword, the resulting expressions can be stored in a variable

		Syntax (Single Statement Execution)
			(expression) ? iftrue :iffalse;
	*/

	//Single Statement Execution
	let ternaryResult = (1 < 18) ? true : false;
	console.log("result of ternary operator: "+ternaryResult);
	console.log(ternaryResult);

	//Multiple Statement execution
	/*
		a function may be defined then used in a ternary operator.
	*/
	let name;
	function isOfLegalAge(){
		name = "John";
		return "you are of the legal age limit";
	}
	function isUnderAge(){
		name = "Jane";
		return "You are under the age limit";
	}
	let age = parseInt(prompt("What is your age?"));
	console.log(age);
	let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge
	console.log("Result of ternary Operator in function: "+legalAge+", "+name);




//Switch Statement
	/*
		The Switch statement evaluate an expression and matches the expression's value to a case clause.

		.toLowerCase() method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case.

		the "expression" is the information used to match the "value" provided in the switch cases.

		Variables are commonly used as expressions to allow varying user input to be used when comparing with seitch case values.

		break statement is used to terminate the current loop once a match has been found.
	*/
	let day = prompt("What day of the week is it today?").toLowerCase();

	switch (day) {
		case "monday":
			console.log("The color of the day is red");
			break;
		case "tuesday":
			console.log("The color of the day is orange");
			break;
		case "wednesday":
			console.log("The color of the day is yellow");
			break;
		case "thurday":
			console.log("The color of the day is green");
			break;
		case "friday":
			console.log("The color of the day is blue");
			break;
		case "saturday":
			console.log("The color of the day is indigo");
			break;
		case "sunday":
			console.log("The color of the day is violet");
			break;
		default:
			console.log("Please input a valid day");
			break;
	}


//Mini Activity 3
	/*
		
	*/

	function determineBear(number){
		switch (number) {
		case 1:
			alert("Hi, I'm Amy!");
			break;
		case 2:
			alert("Hi, I'm Lulu!");
			break;
		case 3:
			alert("Hi, I'm Morgan!");
			break;
		default:
			alert("Please input a valid number");
			break;
		}
	}
	determineBear(2);



//Try-Catch-Finally Statement
	/*
		- Try-Catch-Finally Statement are commonly used for error handling.
		- They are used to specify a responce whenever an error is received.
	*/
	function showIntensityAlert(windSpeed){
		try {
			alerat(determineTyphoonIntensity(windSpeed));
			//error are commonly used variable names used by developers for storing errors.
		} catch (error){
			console.log(typeof error);
			//the error.message is used to access the information relating to an error.
			console.warn(error.message);
		} finally {
			//continue execution of code regardless of success and failure of code execution in the "try" block to handle or resolve errors
			alert("Intensity updates will show you alet.");
		}
	}
	showIntensityAlert(56);